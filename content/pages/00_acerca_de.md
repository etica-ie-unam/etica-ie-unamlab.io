---
title: Comité de Ética
URL:
save_as: index.html
---

## ¿Qué hacemos?

### Dictamen de protocolos de Investigación

Analizamos y dictaminamos aspectos éticos y bioéticos de proyectos de
investigación con animales, plantas, microorganismos, OGM y/o
personas. [Leer más](dictamen-de-proyectos.html).

### Opiniones colegiadas

Brindamos opiniones razonadas sobre problemas éticos o bioéticos de
actividades que se realizan en el Instituto de Ecología, de acuerdo a
los valores universitarios y la ética científica.

### Orientación

Brindamos orientación sobre cómo proceder y a qué instancias acudir
ante casos de falta a la ética y a los valores universitarios,
conflictos por asimetrías de poder, violencia laboral y/o de género.

### Cultura de la ética

Realizamos acciones diversas para promover una cultura de respeto y un
ambiente laboral pacífico de acuerdo a los valores universitarios
fomentando que las actividades en el instituto se desarrollen bajo los
principios de la ética científica y universitaria.
